package simpledb;

/**
 * A class to represent a fixed-width histogram over a single integer-based field.
 */
public class IntHistogram
{
	private final int[] arrayBuckets;
	private final int[] arrayPresumBuckets;
	private final int minValue;
	private final int maxValue;

	private final int rangePerBucket;

	private int numValues = 0;
	private boolean dirty = false;

	/**
	 * Create a new IntHistogram.
	 * <p>
	 * This IntHistogram should maintain a histogram of integer values that it receives.
	 * It should split the histogram into "buckets" buckets.
	 * <p>
	 * The values that are being histogrammed will be provided one-at-a-time through the "addValue()" function.
	 * <p>
	 * Your implementation should use space and have execution time that are both
	 * constant with respect to the number of values being histogrammed.  For example, you shouldn't
	 * simply store every value that you see in a sorted list.
	 *
	 * @param buckets The number of buckets to split the input value into.
	 * @param min     The minimum integer value that will ever be passed to this class for histogramming
	 * @param max     The maximum integer value that will ever be passed to this class for histogramming
	 */
	public IntHistogram(int buckets, int min, int max)
	{
		// some code goes here
		this.arrayBuckets = new int[buckets];
		this.arrayPresumBuckets = new int[buckets];
		this.minValue = min;
		this.maxValue = max;

		this.rangePerBucket = (max - min) / buckets + 1;
	}

	private int getBucketIndex(int v)
	{
		return (v - minValue) / rangePerBucket;
	}

	private void updatePresum()
	{
		if(dirty)
		{
			int sum = 0;
			for (int i = 0; i < arrayBuckets.length; i++)
			{
				sum += arrayBuckets[i];
				arrayPresumBuckets[i] = sum;
			}
			dirty = false;
		}
	}

	/**
	 * Add a value to the set of values that you are keeping a histogram of.
	 *
	 * @param v Value to add to the histogram
	 */
	public void addValue(int v)
	{
		// some code goes here
		arrayBuckets[getBucketIndex(v)]++;
		numValues++;
		dirty = true;
	}

	/**
	 * Estimate the selectivity of a particular predicate and operand on this table.
	 * <p>
	 * For example, if "op" is "GREATER_THAN" and "v" is 5,
	 * return your estimate of the fraction of elements that are greater than 5.
	 *
	 * @param op Operator
	 * @param v  Value
	 * @return Predicted selectivity of this particular operator and value
	 */
	public double estimateSelectivity(Predicate.Op op, int v)
	{
		// some code goes here
		updatePresum();
		switch(op)
		{
		case EQUALS:
		{
			if (v < minValue || v > maxValue)
				return 0.0;
			return (double) arrayBuckets[getBucketIndex(v)] / numValues / rangePerBucket;
		}
		case NOT_EQUALS:
		{
			if (v < minValue || v > maxValue)
				return 1.0;
			return 1.0 - (double) arrayBuckets[getBucketIndex(v)] / numValues / rangePerBucket;
		}
		case LESS_THAN:
		{
			if (v > maxValue)
				return 1.0;
			if (v < minValue)
				return 0.0;

			int bid = getBucketIndex(v);
			double sum = ((v - minValue) % rangePerBucket) / (double)rangePerBucket * (double) arrayBuckets[bid];
			if (bid > 0)
				sum += arrayPresumBuckets[bid - 1];
			return sum / numValues;
		}
		case LESS_THAN_OR_EQ:
		{
			if (v > maxValue)
				return 1.0;
			if (v < minValue)
				return 0.0;

			int bid = getBucketIndex(v);
			double sum = ((v - minValue) % rangePerBucket + 1) / (double)rangePerBucket * (double) arrayBuckets[bid];
			if (bid > 0)
				sum += arrayPresumBuckets[bid - 1];
			return sum / numValues;
		}
		case GREATER_THAN:
		{
			if (v > maxValue)
				return 0.0;
			if (v < minValue)
				return 1.0;

			int bid = getBucketIndex(v);
			double sum = (rangePerBucket - (v - minValue) % rangePerBucket - 1) / (double)rangePerBucket * (double) arrayBuckets[bid];
			sum += numValues - arrayPresumBuckets[bid];
			return sum / numValues;
		}
		case GREATER_THAN_OR_EQ:
		{
			if (v > maxValue)
				return 0.0;
			if (v < minValue)
				return 1.0;

			int bid = getBucketIndex(v);
			double sum = (rangePerBucket - (v - minValue) % rangePerBucket) / (double)rangePerBucket * (double) arrayBuckets[bid];
			sum += numValues - arrayPresumBuckets[bid];
			return sum / numValues;
		}
		default:
			assert(false);
			return 0.0;
		}
	}

	/**
	 * @return the average selectivity of this histogram.
	 * <p>
	 * This is not an indispensable method to implement the basic
	 * join optimization. It may be needed if you want to
	 * implement a more efficient optimization
	 */
	public double avgSelectivity()
	{
		// some code goes here
		return 0.1;
	}

	public double avgSelectivity(Predicate.Op op)
	{
		// some code goes here
		double sum = 0.0;
		for(int i = 0; i < arrayBuckets.length; i++)
		{
			sum += estimateSelectivity(op, minValue + rangePerBucket * i + rangePerBucket / 2);
		}
		return sum / arrayBuckets.length;
	}

	/**
	 * @return A string describing this histogram, for debugging purposes
	 */
	public String toString()
	{
		// some code goes here
		return null;
	}
}

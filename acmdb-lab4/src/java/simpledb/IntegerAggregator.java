package simpledb;

import java.util.HashMap;
import java.util.NoSuchElementException;
import java.util.Iterator;

/**
 * Knows how to compute some aggregate over a set of IntFields.
 */
public class IntegerAggregator implements Aggregator
{
	private static final long serialVersionUID = 1L;

	private final int gbField;
	private final int aggField;
	private final Op aggOp;

	private final TupleDesc tupleDesc;

	protected class AggreateValue
	{
		int sum = 0;
		int max = Integer.MIN_VALUE;
		int min = Integer.MAX_VALUE;
		int count = 0;
	}

	private final HashMap<Field, AggreateValue> mapFieldToAggValue = new HashMap<>();

	AggreateValue aggValueNoGroup = new AggreateValue();

	/**
	 * Aggregate constructor
	 *
	 * @param gbfield     the 0-based index of the group-by field in the tuple, or
	 *                    NO_GROUPING if there is no grouping
	 * @param gbfieldtype the type of the group by field (e.g., Type.INT_TYPE), or null
	 *                    if there is no grouping
	 * @param afield      the 0-based index of the aggregate field in the tuple
	 * @param what        the aggregation operator
	 */

	public IntegerAggregator(int gbfield, Type gbfieldtype, int afield, Op what)
	{
		// some code goes here
		this.gbField = gbfield;
		this.aggField = afield;
		this.aggOp = what;

		if(gbfield == NO_GROUPING)
		{
			Type[] types = {Type.INT_TYPE};
			String[] names = {aggregateFieldName()};
			this.tupleDesc = new TupleDesc(types, names);
		}
		else
		{
			Type[] types = {gbfieldtype, Type.INT_TYPE};
			String[] names = {groupFieldName(), aggregateFieldName()};
			this.tupleDesc = new TupleDesc(types, names);
		}
	}

	/**
	 * Merge a new tuple into the aggregate, grouping as indicated in the
	 * constructor
	 *
	 * @param tup the Tuple containing an aggregate field and a group-by field
	 */
	public void mergeTupleIntoGroup(Tuple tup)
	{
		// some code goes here
		AggreateValue aggValue;
		if(gbField == NO_GROUPING)
		{
			aggValue = mapFieldToAggValue.get(new IntField(0));
			if(aggValue == null)
			{
				aggValue = new AggreateValue();
				mapFieldToAggValue.put(new IntField(0), aggValue);
			}
		}
		else
		{
			aggValue = mapFieldToAggValue.get(tup.getField(gbField));
			if(aggValue == null)
			{
				aggValue = new AggreateValue();
				mapFieldToAggValue.put(tup.getField(gbField), aggValue);
			}
		}
		mergeTuple(tup, aggValue);
	}

	public void reset()
	{
		mapFieldToAggValue.clear();
	}

	public TupleDesc getTupleDesc()
	{
		return tupleDesc;
	}

	public int groupField()
	{
		if(gbField == NO_GROUPING)
			return -1;
		else
			return 0;
	}

	public String groupFieldName()
	{
		if(gbField == NO_GROUPING)
			return null;
		else
			return "Group Value";
	}

	public int aggregateField()
	{
		if(gbField == NO_GROUPING)
			return 0;
		else
			return 1;
	}

	public String aggregateFieldName()
	{
		return aggOp.toString();
	}

	protected void mergeTuple(Tuple entryTuple, AggreateValue aggValue)
	{
		int newValue = ((IntField)entryTuple.getField(aggField)).getValue();
		aggValue.sum += newValue;
		aggValue.count++;
		aggValue.max = Math.max(aggValue.max, newValue);
		aggValue.min = Math.min(aggValue.min, newValue);
	}

	private static int getAggValueByOp(AggreateValue aggValue, Op what)
	{
		switch(what)
		{
		case MIN:
			return aggValue.min;
		case MAX:
			return aggValue.max;
		case SUM:
			return aggValue.sum;
		case AVG:
			return aggValue.sum / aggValue.count;
		case COUNT:
			return aggValue.count;

		default:
			assert(false);
			return 0;
		}
	}

	private class IntegerAggregatorIterator implements DbIterator
	{
		private final IntegerAggregator parent;

		private boolean isOpened = false;
		private Iterator<HashMap.Entry<Field, AggreateValue>> iter;

		IntegerAggregatorIterator(IntegerAggregator parent)
		{
			this.parent = parent;
			this.iter = parent.mapFieldToAggValue.entrySet().iterator();
		}

		@Override
		public void open()
		{
			isOpened = true;
		}

		@Override
		public void close()
		{
			isOpened = false;
		}

		@Override
		public TupleDesc getTupleDesc()
		{
			return parent.tupleDesc;
		}

		@Override
		public Tuple next() throws NoSuchElementException
		{
			HashMap.Entry<Field, AggreateValue> value =  iter.next();
			Field groupByField = value.getKey();
			AggreateValue aggValue = value.getValue();

			Tuple tuple = new Tuple(getTupleDesc());
			if(parent.gbField == NO_GROUPING)
			{
				tuple.setField(0, new IntField(getAggValueByOp(aggValue, parent.aggOp)));
			}
			else
			{
				tuple.setField(0, groupByField);
				tuple.setField(1, new IntField(getAggValueByOp(aggValue, parent.aggOp)));
			}

			return tuple;
		}

		@Override
		public boolean hasNext()
		{
			return iter.hasNext();
		}

		@Override
		public void rewind()
		{
			iter = parent.mapFieldToAggValue.entrySet().iterator();
		}
	}

	/**
	 * Create a DbIterator over group aggregate results.
	 *
	 * @return a DbIterator whose tuples are the pair (groupVal, aggregateVal)
	 * if using group, or a single (aggregateVal) if no grouping. The
	 * aggregateVal is determined by the type of aggregate specified in
	 * the constructor.
	 */
	public DbIterator iterator()
	{
		// some code goes here
		return new IntegerAggregatorIterator(this);
	}

}

package simpledb;

import javax.xml.crypto.Data;
import java.io.*;

import java.util.LinkedList;
import java.util.concurrent.ConcurrentHashMap;
import java.util.Iterator;
import java.util.HashMap;
import java.util.List;

/**
 * BufferPool manages the reading and writing of pages into memory from
 * disk. Access methods call into it to retrieve pages, and it fetches
 * pages from the appropriate location.
 * <p>
 * The BufferPool is also responsible for locking;  when a transaction fetches
 * a page, BufferPool checks that the transaction has the appropriate
 * locks to read/write the page.
 *
 * @Threadsafe, all fields are final
 */
public class BufferPool
{
	/**
	 * Bytes per page, including header.
	 */
	private static final int PAGE_SIZE = 4096;

	private static int pageSize = PAGE_SIZE;

	/**
	 * Default number of pages passed to the constructor. This is used by
	 * other classes. BufferPool should use the numPages argument to the
	 * constructor instead.
	 */
	public static final int DEFAULT_PAGES = 2000;

	private class PageMetadata
	{
		Page storage;
	}

	// private int numPages;
	private final int numMaxPages;
	private final PageMetadata[] arrayPages;
	private final HashMap<PageId, Integer> mapPageIDToPageArray = new HashMap<>();

	// Does anyone know if there's a Linked List in Java which has a *REALLY USABLE* iterator like that in C++? Please tell me.
	private final LinkedList<Integer> listLRU = new LinkedList<>();	// front: least recent, end: most recent

	/**
	 * Creates a BufferPool that caches up to numPages pages.
	 *
	 * @param numPages maximum number of pages in this buffer pool.
	 */
	public BufferPool(int numPages)
	{
		// some code goes here
		// this.numPages = 0;
		this.numMaxPages = numPages;
		this.arrayPages = new PageMetadata[numPages];
		for(int i=0; i<numPages; i++)
		{
			this.arrayPages[i] = new PageMetadata();
			listLRU.add(i);
		}
	}

	public static int getPageSize()
	{
		return pageSize;
	}

	// THIS FUNCTION SHOULD ONLY BE USED FOR TESTING!!
	public static void setPageSize(int pageSize)
	{
		BufferPool.pageSize = pageSize;
	}

	// THIS FUNCTION SHOULD ONLY BE USED FOR TESTING!!
	public static void resetPageSize()
	{
		BufferPool.pageSize = PAGE_SIZE;
	}

	/**
	 * Retrieve the specified page with the associated permissions.
	 * Will acquire a lock and may block if that lock is held by another
	 * transaction.
	 * <p>
	 * The retrieved page should be looked up in the buffer pool.  If it
	 * is present, it should be returned.  If it is not present, it should
	 * be added to the buffer pool and returned.  If there is insufficient
	 * space in the buffer pool, an page should be evicted and the new page
	 * should be added in its place.
	 *
	 * @param tid  the ID of the transaction requesting the page
	 * @param pid  the ID of the requested page
	 * @param perm the requested permissions on the page
	 */
	public Page getPage(TransactionId tid, PageId pid, Permissions perm) throws TransactionAbortedException, DbException
	{
		// some code goes here
		return getPageMetadata(tid, pid, perm).storage;
	}

	private PageMetadata getPageMetadata(TransactionId tid, PageId pid, Permissions perm) throws TransactionAbortedException, DbException
	{
		Integer index = mapPageIDToPageArray.get(pid);
		if(index == null)
		{
			int nextId = evictPage();

			Page page = Database.getCatalog().getDatabaseFile(pid.getTableId()).readPage(pid);
			mapPageIDToPageArray.put(pid, nextId);
			arrayPages[nextId].storage = page;

			listLRU.remove((Integer)nextId);
			listLRU.add(nextId);

			return arrayPages[nextId];
		}

		assert(index >= 0 && index < arrayPages.length);
		listLRU.remove(index);
		listLRU.add(index);
		return arrayPages[index];
	}

	/**
	 * Releases the lock on a page.
	 * Calling this is very risky, and may result in wrong behavior. Think hard
	 * about who needs to call this and why, and why they can run the risk of
	 * calling it.
	 *
	 * @param tid the ID of the transaction requesting the unlock
	 * @param pid the ID of the page to unlock
	 */
	public void releasePage(TransactionId tid, PageId pid)
	{
		// some code goes here
		// not necessary for lab1|lab2
	}

	/**
	 * Release all locks associated with a given transaction.
	 *
	 * @param tid the ID of the transaction requesting the unlock
	 */
	public void transactionComplete(TransactionId tid) throws IOException
	{
		// some code goes here
		// not necessary for lab1|lab2
	}

	/**
	 * Return true if the specified transaction has a lock on the specified page
	 */
	public boolean holdsLock(TransactionId tid, PageId p)
	{
		// some code goes here
		// not necessary for lab1|lab2
		return false;
	}

	/**
	 * Commit or abort a given transaction; release all locks associated to
	 * the transaction.
	 *
	 * @param tid    the ID of the transaction requesting the unlock
	 * @param commit a flag indicating whether we should commit or abort
	 */
	public void transactionComplete(TransactionId tid, boolean commit) throws IOException
	{
		// some code goes here
		// not necessary for lab1|lab2
	}

	/**
	 * Add a tuple to the specified table on behalf of transaction tid.  Will
	 * acquire a write lock on the page the tuple is added to and any other
	 * pages that are updated (Lock acquisition is not needed for lab2).
	 * May block if the lock(s) cannot be acquired.
	 * <p>
	 * Marks any pages that were dirtied by the operation as dirty by calling
	 * their markDirty bit, and adds versions of any pages that have
	 * been dirtied to the cache (replacing any existing versions of those pages) so
	 * that future requests see up-to-date pages.
	 *
	 * @param tid     the transaction adding the tuple
	 * @param tableId the table to add the tuple to
	 * @param t       the tuple to add
	 */
	public void insertTuple(TransactionId tid, int tableId, Tuple t) throws DbException, IOException, TransactionAbortedException
	{
		// some code goes here
		// not necessary for lab1
		// TODO: lock
		List<Page> dirtyPages = Database.getCatalog().getDatabaseFile(tableId).insertTuple(tid, t);
		for(Page page : dirtyPages)
		{
			page.markDirty(true, tid);
			getPageMetadata(tid, page.getId(), Permissions.READ_WRITE).storage = page;
		}
	}

	/**
	 * Remove the specified tuple from the buffer pool.
	 * Will acquire a write lock on the page the tuple is removed from and any
	 * other pages that are updated. May block if the lock(s) cannot be acquired.
	 * <p>
	 * Marks any pages that were dirtied by the operation as dirty by calling
	 * their markDirty bit, and adds versions of any pages that have
	 * been dirtied to the cache (replacing any existing versions of those pages) so
	 * that future requests see up-to-date pages.
	 *
	 * @param tid the transaction deleting the tuple.
	 * @param t   the tuple to delete
	 */
	public void deleteTuple(TransactionId tid, Tuple t) throws DbException, IOException, TransactionAbortedException
	{
		// some code goes here
		// not necessary for lab1
		// TODO
		int tableId = t.getRecordId().getPageId().getTableId();
		List<Page> dirtyPages = Database.getCatalog().getDatabaseFile(tableId).deleteTuple(tid, t);
		for(Page page : dirtyPages)
		{
			page.markDirty(true, tid);
			getPageMetadata(tid, page.getId(), Permissions.READ_WRITE).storage = page;
		}
	}

	/**
	 * Flush all dirty pages to disk.
	 * NB: Be careful using this routine -- it writes dirty data to disk so will
	 * break simpledb if running in NO STEAL mode.
	 */
	public synchronized void flushAllPages() throws IOException
	{
		// some code goes here
		// not necessary for lab1
		for(HashMap.Entry<PageId, Integer> kv : mapPageIDToPageArray.entrySet())
		{
			flushPage(kv.getKey());
		}
	}

	/**
	 * Remove the specific page id from the buffer pool.
	 * Needed by the recovery manager to ensure that the
	 * buffer pool doesn't keep a rolled back page in its
	 * cache.
	 * <p>
	 * Also used by B+ tree files to ensure that deleted pages
	 * are removed from the cache so they can be reused safely
	 */
	public synchronized void discardPage(PageId pid)
	{
		// some code goes here
		// not necessary for lab1
		Integer index = mapPageIDToPageArray.get(pid);
		if(index == null)
			return;

		arrayPages[index].storage = null;
		mapPageIDToPageArray.remove(pid);
		listLRU.remove(index);
		listLRU.addFirst(index);
	}

	/**
	 * Flushes a certain page to disk
	 *
	 * @param pid an ID indicating the page to flush
	 */
	private synchronized void flushPage(PageId pid) throws IOException
	{
		// some code goes here
		// not necessary for lab1
		Integer index = mapPageIDToPageArray.get(pid);
		Page page = arrayPages[index].storage;
		TransactionId tid = page.isDirty();
		if(tid != null)
		{
			Database.getCatalog().getDatabaseFile(pid.getTableId()).writePage(page);
			page.markDirty(false, tid);
		}
	}

	/**
	 * Write all pages of the specified transaction to disk.
	 */
	public synchronized void flushPages(TransactionId tid) throws IOException
	{
		// some code goes here
		// not necessary for lab1|lab2
	}

	/**
	 * Discards a page from the buffer pool.
	 * Flushes the page to disk to ensure dirty pages are updated on disk.
	 * @return the evicted page index in arrayPages
	 */
	private synchronized int evictPage() throws DbException
	{
		// some code goes here
		// not necessary for lab1
		Page page = arrayPages[listLRU.getFirst()].storage;
		if(page == null)
			return listLRU.getFirst();

		try
		{
			flushPage(page.getId());
		}
		catch(IOException e)
		{
			throw new DbException("An I/O error occurred");
		}
		discardPage(page.getId());
		return listLRU.getFirst();
	}

}

package simpledb;

import java.io.Serializable;
import java.util.*;

/**
 * TupleDesc describes the schema of a tuple.
 */
public class TupleDesc implements Serializable
{

	/**
	 * A help class to facilitate organizing the information of each field
	 */
	public static class TDItem implements Serializable
	{

		private static final long serialVersionUID = 1L;

		/**
		 * The type of the field
		 */
		public final Type fieldType;

		/**
		 * The name of the field
		 */
		public final String fieldName;

		public TDItem(Type t, String n)
		{
			this.fieldName = n;
			this.fieldType = t;
		}

		public String toString()
		{
			return fieldName + "(" + fieldType + ")";
		}
	}

	private final ArrayList<TDItem> arrayTDItems = new ArrayList<>();
	private final HashMap<String, Integer> mapFieldNameToID = new HashMap<>();
	private int sizeTuple = 0;

	/**
	 * @return An iterator which iterates over all the field TDItems
	 * that are included in this TupleDesc
	 */
	public Iterator<TDItem> iterator()
	{
		// some code goes here
		return arrayTDItems.iterator();
	}

	private static final long serialVersionUID = 1L;

	/**
	 * Add a field (type, name) to the tuple description
	 *
	 * @param type the type of the field
	 * @param name the name of the field
	 */
	private void addField(Type type, String name)
	{
		sizeTuple += type.getLen();
		arrayTDItems.add(new TDItem(type, name));
		if(name != null)
			mapFieldNameToID.put(name, arrayTDItems.size() - 1);
	}

	/**
	 * Create a new TupleDesc with typeAr.length fields with fields of the
	 * specified types, with associated named fields.
	 *
	 * @param typeAr  array specifying the number of and types of fields in this
	 *                TupleDesc. It must contain at least one entry.
	 * @param fieldAr array specifying the names of the fields. Note that names may
	 *                be null.
	 */
	public TupleDesc(Type[] typeAr, String[] fieldAr)
	{
		// some code goes here
		// arrayTDItems = new TDItem;
		for(int i=0; i<typeAr.length; i++)
		{
			if(i < fieldAr.length)
				addField(typeAr[i], fieldAr[i]);
			else
				addField(typeAr[i], null);
		}
	}

	/**
	 * Constructor. Create a new tuple desc with typeAr.length fields with
	 * fields of the specified types, with anonymous (unnamed) fields.
	 *
	 * @param typeAr array specifying the number of and types of fields in this
	 *               TupleDesc. It must contain at least one entry.
	 */
	public TupleDesc(Type[] typeAr)
	{
		// some code goes here
		assert(typeAr.length >= 1);
		for(Type type : typeAr)
			addField(type, null);
	}

	/**
	 * Constructor. Create an empty tuple desc.
	 */
	private TupleDesc()
	{
	}


	/**
	 * @return the number of fields in this TupleDesc
	 */
	public int numFields()
	{
		// some code goes here
		return arrayTDItems.size();
	}

	/**
	 * Gets the (possibly null) field name of the ith field of this TupleDesc.
	 *
	 * @param i index of the field name to return. It must be a valid index.
	 * @return the name of the ith field
	 * @throws NoSuchElementException if i is not a valid field reference.
	 */
	public String getFieldName(int i) throws NoSuchElementException
	{
		// some code goes here
		if(i < 0 || i >= numFields())
			throw new NoSuchElementException();
		return arrayTDItems.get(i).fieldName;
	}

	/**
	 * Gets the type of the ith field of this TupleDesc.
	 *
	 * @param i The index of the field to get the type of. It must be a valid
	 *          index.
	 * @return the type of the ith field
	 * @throws NoSuchElementException if i is not a valid field reference.
	 */
	public Type getFieldType(int i) throws NoSuchElementException
	{
		// some code goes here
		if(i < 0 || i >= numFields())
			throw new NoSuchElementException();
		return arrayTDItems.get(i).fieldType;
	}

	/**
	 * Find the index of the field with a given name.
	 *
	 * @param name name of the field.
	 * @return the index of the field that is first to have the given name.
	 * @throws NoSuchElementException if no field with a matching name is found.
	 */
	public int fieldNameToIndex(String name) throws NoSuchElementException
	{
		// some code goes here
		Integer index = mapFieldNameToID.get(name);
		if(index == null)
			throw new NoSuchElementException();
		return index;
	}

	/**
	 * @return The size (in bytes) of tuples corresponding to this TupleDesc.
	 * Note that tuples from a given TupleDesc are of a fixed size.
	 */
	public int getSize()
	{
		// some code goes here
		return sizeTuple;
	}

	/**
	 * Merge two TupleDescs into one, with td1.numFields + td2.numFields fields,
	 * with the first td1.numFields coming from td1 and the remaining from td2.
	 *
	 * @param td1 The TupleDesc with the first fields of the new TupleDesc
	 * @param td2 The TupleDesc with the last fields of the TupleDesc
	 * @return the new TupleDesc
	 */
	public static TupleDesc merge(TupleDesc td1, TupleDesc td2)
	{
		// some code goes here
		TupleDesc desc = new TupleDesc();
		for(TDItem item : td1.arrayTDItems)
			desc.addField(item.fieldType, item.fieldName);
		for(TDItem item : td2.arrayTDItems)
			desc.addField(item.fieldType, item.fieldName);
		return desc;
	}

	/**
	 * Compares the specified object with this TupleDesc for equality. Two
	 * TupleDescs are considered equal if they are the same size and if the n-th
	 * type in this TupleDesc is equal to the n-th type in td.
	 *
	 * @param o the Object to be compared for equality with this TupleDesc.
	 * @return true if the object is equal to this TupleDesc.
	 */
	public boolean equals(Object o)
	{
		// some code goes here
		if(!(o instanceof TupleDesc))
			return false;

		TupleDesc rhs = (TupleDesc)o;
		if(numFields() != rhs.numFields())
			return false;
		for(int i = 0; i < numFields(); i++)
		{
			if(arrayTDItems.get(i).fieldType != rhs.arrayTDItems.get(i).fieldType)
				return false;
		}
		return true;
	}

	public int hashCode()
	{
		// If you want to use TupleDesc as keys for HashMap, implement this so
		// that equal objects have equals hashCode() results
		throw new UnsupportedOperationException("unimplemented");
	}

	/**
	 * Returns a String describing this descriptor. It should be of the form
	 * "fieldType[0](fieldName[0]), ..., fieldType[M](fieldName[M])", although
	 * the exact format does not matter.
	 *
	 * @return String describing this descriptor.
	 */
	public String toString()
	{
		// some code goes here
		StringBuilder str = new StringBuilder();
		for(int i = 0; i < numFields(); i++)
		{
			str.append(arrayTDItems.get(i).toString());
			if(i < numFields() - 1)
				str.append(", ");
		}
		return str.toString();
	}
}

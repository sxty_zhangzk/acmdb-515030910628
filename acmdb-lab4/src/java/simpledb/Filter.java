package simpledb;

import java.util.*;

/**
 * Filter is an operator that implements a relational select.
 */
public class Filter extends Operator
{

	private static final long serialVersionUID = 1L;

	private final Predicate pred;
	private DbIterator iter;

	/**
	 * Constructor accepts a predicate to apply and a child operator to read
	 * tuples to filter from.
	 *
	 * @param p     The predicate to filter tuples with
	 * @param child The child operator
	 */
	public Filter(Predicate p, DbIterator child)
	{
		// some code goes here
		this.pred = p;
		this.iter = child;
	}

	public Predicate getPredicate()
	{
		// some code goes here
		return pred;
	}

	public TupleDesc getTupleDesc()
	{
		// some code goes here
		return iter.getTupleDesc();
	}

	public void open() throws DbException, NoSuchElementException, TransactionAbortedException
	{
		// some code goes here
		super.open();
		iter.open();
	}

	public void close()
	{
		// some code goes here
		iter.close();
		super.close();
	}

	public void rewind() throws DbException, TransactionAbortedException
	{
		// some code goes here
		iter.rewind();
	}

	/**
	 * AbstractDbIterator.readNext implementation. Iterates over tuples from the
	 * child operator, applying the predicate to them and returning those that
	 * pass the predicate (i.e. for which the Predicate.filter() returns true.)
	 *
	 * @return The next tuple that passes the filter, or null if there are no
	 * more tuples
	 * @see Predicate#filter
	 */
	protected Tuple fetchNext() throws NoSuchElementException, TransactionAbortedException, DbException
	{
		// some code goes here
		while(true)
		{
			if (!iter.hasNext())
				return null;
			Tuple t = iter.next();
			if(pred.filter(t))
				return t;
		}
	}

	@Override
	public DbIterator[] getChildren()
	{
		// some code goes here
		DbIterator[] iterList = new DbIterator[1];
		iterList[0] = this.iter;
		return iterList;
	}

	@Override
	public void setChildren(DbIterator[] children)
	{
		// some code goes here
		this.iter = children[0];
	}

}

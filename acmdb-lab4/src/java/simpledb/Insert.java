package simpledb;

import java.io.IOException;

/**
 * Inserts tuples read from the child operator into the tableId specified in the
 * constructor
 */
public class Insert extends Operator
{

	private static final long serialVersionUID = 1L;

	private final TransactionId tid;
	private final int tableId;

	private DbIterator iter;

	private boolean done = false;

	private static final TupleDesc tupleDesc = new TupleDesc(new Type[]{Type.INT_TYPE}, new String[]{"Count"});

	/**
	 * Constructor.
	 *
	 * @param t       The transaction running the insert.
	 * @param child   The child operator from which to read tuples to be inserted.
	 * @param tableId The table in which to insert tuples.
	 * @throws DbException if TupleDesc of child differs from table into which we are to
	 *                     insert.
	 */
	public Insert(TransactionId t, DbIterator child, int tableId) throws DbException
	{
		// some code goes here
		this.tid = t;
		this.iter = child;
		this.tableId = tableId;
	}

	public TupleDesc getTupleDesc()
	{
		// some code goes here
		return tupleDesc;
	}

	public void open() throws DbException, TransactionAbortedException
	{
		// some code goes here
		super.open();
		iter.open();
	}

	public void close()
	{
		// some code goes here
		iter.close();
		super.close();
	}

	public void rewind() throws DbException, TransactionAbortedException
	{
		// some code goes here
		iter.rewind();
		done = false;
	}

	/**
	 * Inserts tuples read from child into the tableId specified by the
	 * constructor. It returns a one field tuple containing the number of
	 * inserted records. Inserts should be passed through BufferPool. An
	 * instances of BufferPool is available via Database.getBufferPool(). Note
	 * that insert DOES NOT need check to see if a particular tuple is a
	 * duplicate before inserting it.
	 *
	 * @return A 1-field tuple containing the number of inserted records, or
	 * null if called more than once.
	 * @see Database#getBufferPool
	 * @see BufferPool#insertTuple
	 */
	protected Tuple fetchNext() throws TransactionAbortedException, DbException
	{
		// some code goes here
		if(done)
			return null;

		int cnt = 0;
		while(iter.hasNext())
		{
			Tuple t = iter.next();
			try
			{
				Database.getBufferPool().insertTuple(tid, tableId, t);
			}
			catch(IOException e)
			{
				throw new DbException("An IO Exception occurred: " + e.getMessage());
			}
			cnt++;
		}

		done = true;
		Tuple ret = new Tuple(tupleDesc);
		ret.setField(0, new IntField(cnt));
		return ret;
	}

	@Override
	public DbIterator[] getChildren()
	{
		// some code goes here
		return new DbIterator[]{iter};
	}

	@Override
	public void setChildren(DbIterator[] children)
	{
		// some code goes here
		iter = children[0];
	}
}

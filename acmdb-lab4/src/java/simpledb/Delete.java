package simpledb;

import java.io.IOException;

/**
 * The delete operator. Delete reads tuples from its child operator and removes
 * them from the table they belong to.
 */
public class Delete extends Operator
{

	private static final long serialVersionUID = 1L;

	private final TransactionId tid;

	private DbIterator iter;

	private boolean done = false;

	private static final TupleDesc tupleDesc = new TupleDesc(new Type[]{Type.INT_TYPE}, new String[]{"Count"});

	/**
	 * Constructor specifying the transaction that this delete belongs to as
	 * well as the child to read from.
	 *
	 * @param t     The transaction this delete runs in
	 * @param child The child operator from which to read tuples for deletion
	 */
	public Delete(TransactionId t, DbIterator child)
	{
		// some code goes here
		this.tid = t;
		this.iter = child;
	}

	public TupleDesc getTupleDesc()
	{
		// some code goes here
		return tupleDesc;
	}

	public void open() throws DbException, TransactionAbortedException
	{
		// some code goes here
		super.open();
		iter.open();
	}

	public void close()
	{
		// some code goes here
		iter.close();
		super.close();
	}

	public void rewind() throws DbException, TransactionAbortedException
	{
		// some code goes here
		iter.rewind();
		done = false;
	}

	/**
	 * Deletes tuples as they are read from the child operator. Deletes are
	 * processed via the buffer pool (which can be accessed via the
	 * Database.getBufferPool() method.
	 *
	 * @return A 1-field tuple containing the number of deleted records.
	 * @see Database#getBufferPool
	 * @see BufferPool#deleteTuple
	 */
	protected Tuple fetchNext() throws TransactionAbortedException, DbException
	{
		// some code goes here
		if(done)
			return null;
		int cnt = 0;
		while(iter.hasNext())
		{
			Tuple t = iter.next();
			try
			{
				Database.getBufferPool().deleteTuple(tid, t);
			}
			catch(IOException e)
			{
				throw new DbException("An IO Exception occurred: " + e.getMessage());
			}
			cnt++;
		}
		done = true;

		Tuple ret = new Tuple(tupleDesc);
		ret.setField(0, new IntField(cnt));
		return ret;
	}

	@Override
	public DbIterator[] getChildren()
	{
		// some code goes here
		return new DbIterator[]{iter};
	}

	@Override
	public void setChildren(DbIterator[] children)
	{
		// some code goes here
		iter = children[0];
	}

}

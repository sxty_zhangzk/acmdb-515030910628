package simpledb;

import java.io.*;
import java.util.*;

/**
 * HeapFile is an implementation of a DbFile that stores a collection of tuples
 * in no particular order. Tuples are stored on pages, each of which is a fixed
 * size, and the file is simply a collection of those pages. HeapFile works
 * closely with HeapPage. The format of HeapPages is described in the HeapPage
 * constructor.
 *
 * @author Sam Madden
 * @see simpledb.HeapPage#HeapPage
 */
public class HeapFile implements DbFile
{
	private final File heapFile;
	private final TupleDesc tupleDesc;

	/**
	 * Constructs a heap file backed by the specified file.
	 *
	 * @param f the file that stores the on-disk backing store for this heap
	 *          file.
	 */
	public HeapFile(File f, TupleDesc td)
	{
		// some code goes here
		heapFile = f;
		tupleDesc = td;
	}

	/**
	 * Returns the File backing this HeapFile on disk.
	 *
	 * @return the File backing this HeapFile on disk.
	 */
	public File getFile()
	{
		// some code goes here
		return heapFile;
	}

	/**
	 * Returns an ID uniquely identifying this HeapFile. Implementation note:
	 * you will need to generate this tableid somewhere ensure that each
	 * HeapFile has a "unique id," and that you always return the same value for
	 * a particular HeapFile. We suggest hashing the absolute file name of the
	 * file underlying the heapfile, i.e. f.getAbsoluteFile().hashCode().
	 *
	 * @return an ID uniquely identifying this HeapFile.
	 */
	public int getId()
	{
		// some code goes here
		return heapFile.getAbsoluteFile().hashCode();
	}

	/**
	 * Returns the TupleDesc of the table stored in this DbFile.
	 *
	 * @return TupleDesc of this DbFile.
	 */
	public TupleDesc getTupleDesc()
	{
		// some code goes here
		return tupleDesc;
	}

	// see DbFile.java for javadocs
	public Page readPage(PageId pid)
	{
		// some code goes here
		assert(pid instanceof HeapPageId);

		int sizePage = BufferPool.getPageSize();

		// I want RAII !!!
		RandomAccessFile ramFile = null;
		try
		{
			ramFile = new RandomAccessFile(heapFile, "r");
			if(ramFile.length() < (pid.pageNumber() + 1) * sizePage)
				throw new IllegalArgumentException();
			byte[] bytes = new byte[sizePage];
			ramFile.seek(pid.pageNumber() * sizePage);
			ramFile.read(bytes, 0, sizePage);
			ramFile.close();

			return new HeapPage((HeapPageId)pid, bytes);
		}
		catch(IOException e)
		{
			if(ramFile != null)
			{
				try
				{
					ramFile.close();
				}
				catch(IOException e2) { }
			}
			throw new IllegalArgumentException();
		}
	}

	// see DbFile.java for javadocs
	public void writePage(Page page) throws IOException
	{
		// some code goes here
		// not necessary for lab1
		assert(page.getId().getTableId() == getId());

		int sizePage = BufferPool.getPageSize();

		RandomAccessFile ramFile = null;
		try
		{
			ramFile = new RandomAccessFile(heapFile, "rw");
			ramFile.seek(page.getId().pageNumber() * sizePage);
			ramFile.write(page.getPageData(), 0, sizePage);
			ramFile.close();
		}
		catch(IOException e)
		{
			if(ramFile != null)
			{
				ramFile.close();
			}
			throw e;
		}
	}

	/**
	 * Returns the number of pages in this HeapFile.
	 */
	public int numPages()
	{
		// some code goes here
		return (int)(heapFile.length() / BufferPool.getPageSize());
	}

	// see DbFile.java for javadocs
	public ArrayList<Page> insertTuple(TransactionId tid, Tuple t) throws DbException, IOException, TransactionAbortedException
	{
		// some code goes here
		ArrayList<Page> dirtyPages = new ArrayList<>();
		for(int i=0; i < numPages(); i++)
		{
			HeapPageId pid = new HeapPageId(getId(), i);
			HeapPage page = (HeapPage)Database.getBufferPool().getPage(tid, pid, Permissions.READ_WRITE);
			if(page.getNumEmptySlots() > 0)
			{
				page.insertTuple(t);
				page.markDirty(true, tid);
				dirtyPages.add(page);
				return dirtyPages;
			}
		}

		HeapPageId pid = new HeapPageId(getId(), numPages());
		int sizePage = BufferPool.getPageSize();

		RandomAccessFile ramFile = new RandomAccessFile(heapFile, "rw");
		byte[] bytes = new byte[sizePage];
		ramFile.seek(pid.pageNumber() * sizePage);
		ramFile.write(bytes, 0, sizePage);

		Database.getBufferPool().discardPage(pid);
		HeapPage page = (HeapPage)Database.getBufferPool().getPage(tid, pid, Permissions.READ_WRITE);
		page.insertTuple(t);
		page.markDirty(true, tid);
		dirtyPages.add(page);
		return dirtyPages;
		// not necessary for lab1
	}

	// see DbFile.java for javadocs
	public ArrayList<Page> deleteTuple(TransactionId tid, Tuple t) throws DbException, TransactionAbortedException
	{
		// some code goes here
		ArrayList<Page> dirtyPages = new ArrayList<>();
		PageId pid = t.getRecordId().getPageId();

		if(pid == null || pid.getTableId() != getId())
			throw new DbException("Tuple is not a member of this heap file");

		HeapPage page = (HeapPage)Database.getBufferPool().getPage(tid, pid, Permissions.READ_WRITE);
		page.deleteTuple(t);

		dirtyPages.add(page);
		return dirtyPages;
		// not necessary for lab1
	}

	private class HeapFileIterator implements DbFileIterator
	{
		private final HeapFile heapFile;
		private final TransactionId tid;
		private HeapPageId pageId;
		private Iterator<Tuple> iterInPage;

		public HeapFileIterator(HeapFile heapFile, TransactionId tid)
		{
			this.heapFile = heapFile;
			this.tid = tid;
			this.pageId = new HeapPageId(heapFile.getId(), 0);
			this.iterInPage = null;
		}

		@Override
		public void open() throws DbException, TransactionAbortedException
		{
			Page page = Database.getBufferPool().getPage(tid, pageId, null);
			assert(page instanceof HeapPage);
			iterInPage = ((HeapPage)page).iterator();
		}

		@Override
		public void close()
		{
			iterInPage = null;
		}

		@Override
		public boolean hasNext() throws DbException, TransactionAbortedException
		{
			if(iterInPage == null)
				return false;

			while(true)
			{
				if (iterInPage.hasNext())
					return true;

				if (pageId.pageNumber() < heapFile.numPages() - 1)
				{
					pageId = new HeapPageId(heapFile.getId(), pageId.pageNumber() + 1);
					open();
				}
				else
					return false;
			}
		}

		@Override
		public Tuple next() throws DbException, TransactionAbortedException, NoSuchElementException
		{
			if(!hasNext())
				throw new NoSuchElementException();
			return iterInPage.next();
		}

		@Override
		public void rewind() throws DbException, TransactionAbortedException
		{
			if(iterInPage == null)
				throw new IllegalStateException();

			pageId = new HeapPageId(heapFile.getId(), 0);
			open();
		}
	}

	// see DbFile.java for javadocs
	public DbFileIterator iterator(TransactionId tid)
	{
		// some code goes here
		return new HeapFileIterator(this, tid);
	}

}

